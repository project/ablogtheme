<?php 
?><div class="templatemo_section_box">
  <div class="templatemo_section_box_top">
    <?php if ($block->subject): ?><h1><?php print $block->subject; ?></h1><?php endif; ?>
  </div>
  <div class="templatemo_section_box_mid">
    <?php print $block->content; ?>
    <div class="clear-block"> </div>
  </div>
  <div class="templatemo_section_box_bottom"> </div>
</div>
