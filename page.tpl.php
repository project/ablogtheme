<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--  Designed by w w w . t e m p l a t e m o . c o m  --> 
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language; ?>">
<head>
<?php print $head; ?>
<?php print $styles; ?>
<?php print $scripts; ?>
<title><?php print $head_title; ?></title>
</head>
<body>
	<div id="templatemo_background_section_top">
    
    	<div class="templatemo_container">
		
        	<div id="templatemo_header">
				<div id="templatemo_logo_section">            
		        	<h1><a href="<?php print check_url($base_path); ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>            
					<?php if($site_slogan): ?><h2><?php print $site_slogan; ?></h2><?php endif; ?>
				</div>
				<?php if ($search_box): ?>
                  <div id="templatemo_search_box">
	                <?php print $search_box; ?>
                  </div>
                 <?php endif; ?>
         	</div><!-- end of header -->
            
            <?php if (!empty($p_links)) : ?>
    		  <div id="templatemo_menu_panel">
    			<div id="templatemo_menu_section">
            		<ul>
		                <?php print $p_links; ?>                 
		            </ul> 
				</div>               
		      </div> <!-- end of menu -->
		    <?php endif; ?>
            
		</div><!-- end of container-->
        
	</div><!-- end of templatemo_background_section_top-->
    
    <div id="templatemo_background_section_middle">
    
    	<div class="templatemo_container">
        
        	<div id="templatemo_left_section">     
              <?php if (!empty($breadcrumb)): ?><?php print $breadcrumb; ?><?php endif; ?>
		      <?php if (!empty($title) && empty($node)): ?><h2><?php print $title; ?></h2><?php endif; ?>
              <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
      		  <?php if (!empty($messages)): print $messages; endif; ?>
      		  <?php if (!empty($help)): print $help; endif; ?>
              <?php if ($is_front && !empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
           	  <?php print $content; ?>    
                
            </div><!-- end of left section-->
            
            <?php if (!empty($sidebar_right)): ?>
              <div id="templatemo_right_section">
			    <?php print $sidebar_right; ?>
              </div><!-- end of right Section -->
            <?php endif; ?>
            
        </div><!-- end of container-->
	</div><!-- end of background middle-->
    <?php if (!empty($footer_message)): ?>
    <div id="templatemo_bottom_panel">
      
    	<div id="templatemo_bottom_section">	
    	  <?php print $footer_message; ?>
    	 <div class="clear-block"> </div>
        </div>
      
       
    </div>
     <?php endif; ?>
   <div id="templatemo_footer_section">
         <?php print $footer_msg; ?> | Designed by <a href="http://www.templatemo.com/">TemplateMo.com</a> | <a href="http://topdrupalthemes.net/">Drupal Themes</a> | <a href="http://phpweby.com/hostgator_coupon.php">hosting coupon</a>
       </div>
    <?php print $closure; ?>
</body>
</html>
