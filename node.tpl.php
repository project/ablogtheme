<?php
?>
<div class="templatemo_post" id="post-<?php print $node->nid; ?>">
  <div class="templatemo_post_top">
    <h1><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></h1>
  </div>
  <div class="templatemo_post_mid">
    <?php print $content; ?> 
 
    <?php if ($terms): ?>
      <div class="post"><?php print t('Tags'); ?>: <?php print $terms; ?></div> 
    <?php endif; ?>
    <div class="post">
       <?php print $posted_by; ?>
       <?php if ($links): ?> | <?php  print $links; ?><?php endif; ?>
     </div>
  <div class="clear-block"> </div>

</div>

</div>

