<?php

function ablogtheme_preprocess_page(&$vars) {

  $vars['p_links'] = '';
  if (!empty($vars['primary_links'])) {

    foreach ($vars['primary_links'] as $link) {
      $link_current = '';
      $href = url($link['href']);
      if ($link['href'] == '<front>' && drupal_is_front_page()) {
        $link_current = 'current';
      }

      if ($link['href'] == $_GET['q']) {
        $link_current = 'current';
      }
       
      $vars['p_links'] .= '<li><a href="' . $href . '" class="' . $link_current . '"><span class="left">' . $link['title'] . '</span></a></li>';
    }
  }
  

  $vars['footer_msg'] = ' &copy; ' . $vars['site_name'] . ' ' . date('Y');
  $vars['search_box'] = str_replace(t('Search this site: '), '', $vars['search_box']);
}

function ablogtheme_preprocess_node(&$vars) {
  $vars['postdate'] = format_date($vars['node']->created, 'custom', 'd F Y');
  $vars['author'] = theme('username', $vars['node']);
  $vars['posted_by'] = t('By') . ' ' . $vars['author'] . ' ' . t('on') . ' ' . $vars['postdate'];
}

function ablogtheme_preprocess_comment(&$vars) {
  $vars['classes'] = 'comment';
}

function ablogtheme_regions() {
  return array('right' => t('Right'), 'content'  => t('Content'), 'footer' => t('Footer'));
}

function _phptemplate_variables($hook, $vars) {

  if($hook == 'comment') {
    $vars['node'] = node_load($vars['comment']->nid);
  	ablogtheme_preprocess_comment($vars);
  }
  else {
    $function = 'ablogtheme_preprocess_' . str_replace('-', '_', $hook);
    if (function_exists($function)) 
	$function($vars);
  }

  return $vars;
}

function ablogtheme_comment_wrapper($content) {

    return '<div id="comments"><h3>' . t('Comments on this post') . '</h3> <ol class="commentlist">' . $content . '</ol></div>';
}



